# Creating a character with Character Generator and animating it with Mechanim Animation System #

We are going to generate a character with Autodesk Character Generator, this character will later be imported to unity and animated with Mecanim Animation System.

## Downloading source for Tutorial ##

Either you clone the repo at `https://kongos@bitbucket.org/kongos/mechanim-tutorial.git` or you download it as a [zip](https://bitbucket.org/kongos/mechanim-tutorial/get/master.zip). If you downloaded it as a zip, extract it somewhere convenient.

##Create your own character##

1. Go to [Character Generator](https://charactergenerator.autodesk.com/), create an account and log in.
2. Press new as seen here
![Bild1](http://i.imgur.com/ZJyDORd.png)
3. Blend your character using the sliders here.
![Bild2](http://i.imgur.com/NmMHkMD.png)
4. You can assign different types of face, skin, eye, hair, body and clothes style.
![Bild3](http://i.imgur.com/fgzIhHa.png)
5. Now you will be back at **My Characters** and to generate the newwly created character press the Generate button. In the popup press Options
![Bild4](http://i.imgur.com/u85E7Sb.png)
6. In the options pane, you can define height et cetera. After the desired options have been made, press **Generate**.
![Bild5](http://i.imgur.com/aah9RYr.png)
7. After a while the character has been generated, then go to **Generated Characters** and press **Download**.
![Bild6](http://i.imgur.com/6VoQ5gA.png)
8. When the character has been downloaded, extract it into your workspace.

Now you have completed the character generation. Should you anytime want to generate a new character redo the steps.

## Importing and rigging the character in Unity ##

To make sure that the character will work as it should in Unity, it needs to be rigged correctly.

Should you encounter any problems, make sure that the inspector in Unity is not set to `Debug mode`

1. Start Unity and load up the project workspace i.e. `MecanimTute`. Open the Scene called Tutorial-start.unity
2. Find the character amongst the assets, press the Rig button. Choose animation type as `Humanoid`. Avatar definition should say `Create from this model`. Choose `Optimize Game Objects`. The inspector should now look something like this. Press the **Configure** button.
![Rigging](http://i.imgur.com/HZGfGpU.png)
3. Now the Scene view should change and you should be presented with a rigged body, to make sure it works, go to **Muscles** and pull the sliders and watch your character move accordingly.
![Rig](http://i.imgur.com/OnnPtes.png)
Does it look good? In that case press **Done**, now your character has a Humanoid skeleton and you can apply standard animations through character controllers.

## Creating an animation controller ##

We have supplied this tutorial with a set of animations, but we need to define how these animations are to be played out and how their states are to change. An indepth manual on the Unity Mechanim Animation System can be found here: [Unity - Manual: Mechanim Animation System](http://docs.unity3d.com/Manual/MecanimAnimationSystem.html).

Upon completion 

1. The character is going to be able to perform seven different animations and smoothly make transitions inbetween them.

2. There is also going to be some external input to the animations. 

In Mechanim animations are performed in states that can be controlled by variables, lets have a look.

### First of all ###

Before anything can be done we need to create an `Avatar Mask` do this in your project by right clicking the mouse in the project pane, in the context menu choose  Create --> Avatar Mask. as seen here.
![Mask](http://i.imgur.com/L8IrSh7.png) 

In the Avatar Mask make sure that only the arms are green. This means that we can mask away the lower body parts later on.

### Creating the controller ###

Mechanims animation controllers consists of `layers` and `states`. States can consist of blendtrees and between states there are transistions. You will see this later. The only important thing to remember is that you will need to *remember the names of the layers, states and variables you create*. Therefor it is good practice to wisely choose names for these entities.

To create an `Animation Controller` right clicking the mouse in the project pane, in the context menu choose  Create --> Animation Controller. Set a name for your controller and open it up. A good name could be the name of your character concatenated with Controller.

#### The wave ####

To start of and seeing the use of the newly created `Avatar Mask` we will now define how the wave will behave.

1. Add a new layer by pressing the **+** as seen here
![Add layer](http://i.imgur.com/Y1W211C.png) 
Name the layer `Layer2` and in `Mask` set the newly defined `Avatar Mask`

2. In the down left corner of the *Animator* view press the **+** besides `Parameters` and set a new Bool naming it Wave.  

3. Right click in the *Animator* view and press Create State --> Empty. Press the newly created state and name it `Nothing`. Make sure that Foot IK is checked.

4. Create a new empty state and name it `Wave`. In Motion set the animation motion to be Wave.

5. Right click the `Nothing` state and choose `Set As Default`. Now right click it again, now choosing `Make Transition` You will now see an arrow going from the `Nothing` state towards your pointer. Set the pointer above `Wave` and left click. Now the two states are connected with a transition. But we need a returing transition as well, so create a transition fom `Wave` to `Nothing`.

6. Click the Transition arrow going from `Nothing` to `Wave` and assign **Atomic to true**, **Transition Duration to 0.1** and **Condition to Wave true**. Click the arrow going from `Wave` to `Nothing` assign **Condition to Exit Time 0.93**. This tells the Mechanim engine when and how to perform the transitions.

The stategraph of `Layer2` should now look like this.

![Layer2](http://i.imgur.com/UmOHLvP.png) 

Go to the Base Layer in your Animation Controller, create a new empty state, name this `Idle`. Idle should have `Motion` set to Idle and Foot IK should be true. This means that the default behavior for your character will be to perform the idle motion and Foot IK makes sure that it will be grounded.

Now lets try and see if it works.

1. Find the character you previously created. Drag the character out into the scene view and place it somewhere you see fit.

2. Create a new Empty GameObject and assign it as a child to your character. Name the GameObject `CamPos` and give the **Transform** of this GameObject the **Position : {0,3,-3} and Rotation : {20, 0, 0}** 

3. In the Hiearchy view choose your character, we will now look at the Inspector. In the Animator component set the Controller to the one you previously created. 

4. Now press the big button **Add Component** furthest down in the Inspector. By using the search bar Add a **Rigidbody** a **Capsule Collider** and an **Anim Triggers**. Look at the name of your character and figure out a name for the script that is to control the state transitions. *I named mine `OttoControl` since Otto is the name of my character.* Since no Component exists that has the name you typed in, either press the `New Script` or press Enter on the keyboard. Now set your preferred language. *I will type in C# but you can also use JavaScript or Boo.* **Should you use anything but C# you need to change the syntax accordingly.** After the language is set press **Create and Add.** 

5. Now take a look at the values in my Inspector and assign accordingly in yours. If you have another height or your character is built differently then mine change **Rigidbody Mass**, **Capsule Collider Radius** and **Height** accordingly.
![ottoinsp](http://i.imgur.com/M2vuSNM.png) 

6. Open up the newly created controller script. By double clicking it, it should open up in MonoDeveloper. While you wait for it to open up open [Unity API reference](http://docs.unity3d.com/ScriptReference/) in a new tab/ window.

7. In the newly opened controller script. Add a private Animator like,

		private Animator anim;	// a reference to the animator on the character


Change the Start function to:

	void Start ()
		{
			// initialising reference variables
			anim = GetComponent<Animator>();					  
			if(anim.layerCount == 2)
				anim.SetLayerWeight(1, 1);
		}

This tells the Animator that we have equal weight between the layers.

Remove the Update function and add this:

	void FixedUpdate ()
		{
			if(Input.GetButtonUp("Jump"))
				anim.SetBool("Wave", true);
		}


Save and wait to see if any errors were made.

Press play and watch the character wave as you hit space.
![ottoinsp](http://i.imgur.com/nuadfTF.png)

#### The other animations ####
##### Walk Back #####
Now its time for all the other animations.

1. Open up your `Animation Controller` once more. In `Base Layer` right click and choose `Create State` --> `From New Blend Tree`. Name this new state `WalkBack` and make sure that Foot IK is true. Connect the new state back and forth to `Idle`.

2. The transition arrow from `Idle` to `WalkBack` should be atomic and use a Condition of **Speed Less -0.1**, the returning arrow should also be atomic and use the Condition **Speed Greater -0.1**

3. Open `WalkBack` by clicking it. Create a new `Parameter` of type `Float` named `Direction`. 	In the inspector for the Blend Tree make sure this is the parameter used. 

4. Now under Motion in the Inspector add three motions by clicking **+** --> Add Motion Field. A slider will appear above the Motion Field. Click the minimum value of `0` and assign `-1`. 

5. In the Motion Field Assign motions in this order **{WalkBackTurnLeft, WalkBack, WalkBackTurnRight}.** **Automate Thresholds should be true** and **Adjust Time Scale on Select.** 

6. In the down right corner of the screen there is a animation viewer if there is no model present press the little upper body and assign **Auto**. 

7. Press Play in the animation viewer and the model will begin walking backwards. If you pull the slider in the Blend Tree you can watch the model turn accordingly.
It should now look like this.
![Walk back](http://i.imgur.com/9sUBn3X.png)

##### Locomotion #####

1. Go back to the Base Layer of your `Animation Controller` and create a new state housing a new blend tree. Name this state `Locomotion` and make sure that Foot IK is true. Connect the state back and forth to the `Idle` state and open it up.

2. The transition arrow from `Idle` to `Locomotion` should be atomic and use a Condition of **Speed Greater 0.1**, the returning arrow should also be atomic and use the Condition **Speed Less 0.1**

3. Create a new `Parameter` of type Float and name it `Speed`.

4. Click the Blend tree so it shows in the Inspector. Set the Blend Parameter to `Speed`. Now under Motion add two Blend Trees by clicking **+** --> New Blend Tree. The parameter should go from 0 --> 0.5

5. Click the upper of the newly created Blend Trees and name it `Walks`. Make sure it blends with the parameter `Direction`. Add the tree motions {WalkFwdTurnLeft, WalkForward, WalkFwdTurnRight} and make sure the parameter blends from -1 to 1 with Automate Threshold set to true. 

6. Now choose the other of the Blend Trees and name it `Runs`. Assign this Blend Tree the motions **{RunLeft, Run, RunRight}** and use the same parameter settings as before. 

7. Choose the `Blend Tree` with two slides and play in the animation viewer. While you adjust both sliders you can see how the animation changes speed and direction smoothly.

The graph of the Locomotion should look something like this.

![Locomotion](http://i.imgur.com/MWmj2tF.png)

##### Jump #####

1.  Go back to the Base Layer and create a new empty state. Name this state `Jump` and make sure that Foot IK is true. Connect the state back and forth to the `Locomotion` state. 

2. Assign the Jump animation as the motion for this state.

3. The transition arrow from `Locomotion` to `Jump` should be atomic and use a Condition of **Jump true**, the returning arrow should also be atomic and use the Condition **Exit Time 0.89** 

##### JumpDown sequence #####

1.  Go back to the Base Layer and create a new empty state. Name this state `JumpDown` and make sure that Foot IK is false. Make a transition from `Locomotion` state to this `JumpDown` state. 

2. Assign the JumpDown animation as the motion for this state.

3. Create a new Bool Parameter named `JumpDown`. 

4. The transition arrow to `JumpDown` should be atomic and use the Condition **JumpDown true**.

5. Create a new empty state named `Falling`, assign it Fall as animation. It should transition from `JumpDown` to `Falling`, be atomic and have **Exit Time 0.61** as condition.  

6. Create a new empty state, name it `Roll` and connect it inbound from `Falling` (the transition will automatically use Exit Time 0). Give `Roll` the Roll animation. Now make a transition from `Roll` all the way back to `Locomotion`. The transition shall be atomic and use **Exit Time 0.9** 

#### Final look of Animation Controller ####

After all this work with the controller it should look something like this.

![Final Look](http://i.imgur.com/C3LTKhY.png)

#### Connecting the controller with code ####

Now we have defined our `Animation Controller` but without any code it will not do us any good. Thus, code.
But before we code we need to define some new Parameters in our `Animation Controller`.

1. Create a new Float Parameter in your `Animation Controller` and name it `ColliderY`.

2. Create a new Float Parameter in your `Animation Controller` and name it `ColliderHeight`.

3. Find your controller script and open it. Now its time to add code. 

4.  Have a look at the script `OttoControl.cs` and read the comments and make sure you anderstand how it works, do not forget to consult the [Unity API](http://docs.unity3d.com/ScriptReference/)

5. Make adjustments to your controller script, save it and wait for it to compile and make sure there are no erros. 

6. Press Play and run around the area with **{W,A,S,D}** and jump with **{Space}**. It will look something like this:
![Done](http://i.imgur.com/lTYQlk6.png)

You are now done, should there be more time, try to make the transitions smoother.

## I have found an issue ##
1. Tell the teacher
2. If you can fix the issue, do it and send a pull request. If you cannot fix the issue or you dont have time to do so file an [issue](https://bitbucket.org/kongos/mechanim-tutorial/issues?status=new&status=open).

## Thanks ##
This tutorial was based on content from.
Unity 4.0 Mechanim Animation Tutorial [link](https://www.youtube.com/watch?v=Xx21y9eJq1U) and their supplied [source](http://files.unity3d.com/will/MecanimTute.zip)